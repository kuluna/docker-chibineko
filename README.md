Chibineko for Docker
====

Chibineko ([https://github.com/tabbyz/chibineko](https://github.com/tabbyz/chibineko)) running on docker.

# Docker hub
[https://hub.docker.com/r/kuluna/chibineko/](https://hub.docker.com/r/kuluna/chibineko/)

# Run

```
docker run -d -p 3000:3000 kuluna/chibineko
```

# LICENSE
MIT License